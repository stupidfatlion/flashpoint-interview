External dependencies: pycurl

You can either run old_classicar.py like so:


```
#!bash

python old_classicar.py
```


Or you can import it and run it from the command line:


```
#!bash

python -c 'from old_classicar import Crawler_Old_Classicar; Crawler_Old_Classicar("http://www.oldclassiccar.co.uk/forum/phpbb/phpBB2/viewtopic.php?t=12591").main()'
```
