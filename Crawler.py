from __future__ import unicode_literals
import pycurl
import csv
from time import sleep
import cStringIO

class Crawler:
    def __init__(self, seed_url, agent=None, proxy=None, file_name=None):
        self.file_name = file_name if file_name else "forum.csv"
        self.seed_url = seed_url
        self.current_page = 1
        self.is_next_page_available = False
        self.pause_timer = 0

        self.c = pycurl.Curl()
        if agent:
            self.c.setopt(pycurl.USERAGENT, agent)
        if proxy:
            self.c.setopt(pycurl.PROXY, proxy)
        self.c.setopt(pycurl.COOKIEFILE, "")
        self.fields = ["post_id", "name", "date_of_post", "post_body", "post_body_html"]

        with open(self.file_name, "w") as csv_file:
            csv_file.write(",".join(self.fields)+"\n")


    def artificial_actions(func):
        def func_wrapper(self, *args, **kwargs):
            print "Sleeping", self.pause_timer
            sleep(self.pause_timer)
            return func(self, *args, **kwargs) 
        return func_wrapper

    #overload this
    def parse_page(self, html_text):
        #checks if next page is available by using current_page + 1
        #yields each value to write to a dictionary
        #re-initiate the self.seed_url
        return {}

    @artificial_actions
    def get_current_page(self):
        self.pause_timer = 0
        print "Getting page:", self.current_page
        buffer = cStringIO.StringIO()
        self.c.setopt(self.c.URL, self.seed_url)
        self.c.setopt(self.c.WRITEFUNCTION, buffer.write)
        self.c.perform()
        return buffer.getvalue()

    def write_to_csv(self, data_entry):
        with open(self.file_name, "a") as csv_file:
            word_count = len(data_entry["post_body"].split())
            self.pause_timer += word_count * .03
            writer = csv.DictWriter(csv_file, fieldnames=self.fields, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(data_entry)
    
    def main(self):
        try:
            while True:
                html_text = self.get_current_page()
                map(self.write_to_csv, self.parse_page(html_text))
                if not self.is_next_page_available:
                    print "no more"
                    break
        except KeyError:
            print "Key Error, are you sure we're pasing the right stuff?"
            

            
        

