from __future__ import unicode_literals
from Crawler import Crawler
import cStringIO
from bs4 import BeautifulSoup
import bs4

class Crawler_Old_Classicar(Crawler):
    def parse_page(self, html_text):
        soup = BeautifulSoup(html_text.decode("utf-8", "ignore"), "html5lib")
        for post in [tr for tr in soup.find("table", {"class":"forumline"}).findAll("tr") if tr.find("span", {"class":"name"})]:
            parsed_post = {
                    "post_id":None,
                    "name":None,
                    "date_of_post":None,
                    "post_body":None,
                    }
            header = post.find("span", {"class":"name"})
            if header:
                parsed_post["name"] = header.text
                parsed_post["post_id"] = header.find("a")["name"]
            post_details = post.findAll("span", {"class":"postdetails"})[-1]
            if post_details:
                date = [l for l in post_details if isinstance(l, bs4.element.NavigableString)][0].split("Posted: ")[-1]
                parsed_post["date_of_post"] = date 
            post_body = post.find("tbody").findAll("tr")[2:]
            #you would have to add a more specific parser for quoted lines and whatnot
            parsed_post["post_body_html"] = str(post_body[0])
            parsed_post["post_body"] = post_body[0].text
            yield parsed_post


        self.is_next_page_available = False
        for link in soup.findAll("span", {"class":"nav"})[-2].findAll("a"):
            if link.text.strip() == str(self.current_page+1):
                self.seed_url = "/".join(self.seed_url.split("/")[:-1]) 
                self.seed_url += "/" + link["href"]
                self.is_next_page_available = True
                self.current_page +=1
                break

        

if __name__ == "__main__":
    Crawler_Old_Classicar("http://www.oldclassiccar.co.uk/forum/phpbb/phpBB2/viewtopic.php?t=12591").main()
    
